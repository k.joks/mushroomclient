
// Functions for communicating with backend-API here...

function fetchMushroom() {
    return fetch(
        `${API_URL}/seened`,
        {
            method: 'GET'
        }
    )
    .then(response => response.json());
}

function postSomething(something) {
    return fetch(
        `${API_URL}/something`, 
        {
            method: 'POST', 
            body: JSON.stringify(something)
        }
    );
}

function deleteSomething(id) {
    return fetch(
        `${API_URL}/something/${id}`,
        {
            method: 'DELETE'
        }
    );
}
